@extends('layouts.app')
@section('title', 'Edit Category')
@section('content')
<div class="container">
    <div class="row p-5">
        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-header">Edit Category</div>
                <div class="card-body">
                    <form action="{{ route('categories.update', $category->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" name="name" class="form-control" value="{{ $category->name }}">
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" name="is_publish" class="form-check-input" id="is_publish" value="1" {{ $category->is_publish ? 'checked' : '' }}>
                            <label class="form-check-label" for="is_publish">Publish</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a href="{{ route('categories.index') }}" class="btn btn-warning"><i class="fas fa-home"></i></a>
                    </form>
               </div>
            </div>
        </div>
    </div>
</div>
@endsection
