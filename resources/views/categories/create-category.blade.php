@extends('layouts.app')
@section('title', 'Create Category')
@section('content')
<div class="container mt-5">
    <div class="row">
      <div class="col-md-6 mx-auto">
        <h1>Create Category</h1>
        <form method="POST" action="{{ route('categories.store') }}">
            @csrf
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" required>
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="is_publish" name="is_publish" value="1" {{ old('is_publish') ? 'checked' : '' }}>
                <label class="form-check-label" for="is_publish">Publish</label>
            </div>
            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>
</div>
@endsection
