@extends('layouts.app')
@section('title', 'Show Category')
@section('content')
<div class="container">
    <div class="row p-5">
        <div class="col-md-5 mx-auto">
            <div class="card">
                <div class="card-header">Category Detail</div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="col-md-12 control-label">Name:</label>
                        <div class="col-md-12">
                            <p class="form-control-static">{{ $category->name }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="is_publish" class="col-md-12 control-label">Publish:</label>
                        <div class="col-md-12">
                            <p class="form-control-static">{{ $category->is_publish ? 'Yes' : 'No' }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="created_at" class="col-md-12 control-label">Created At:</label>
                        <div class="col-md-12">
                            <p class="form-control-static">{{ $category->created_at }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="updated_at" class="col-md-12 control-label">Updated At:</label>
                        <div class="col-md-12">
                            <p class="form-control-static">{{ $category->updated_at }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-4">
                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                            <form action="{{ route('categories.destroy', $category->id) }}" method="POST" style="display: inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                            </form>
                            <a href="{{ route('categories.index') }}" class="btn btn-warning"><i class="fas fa-home"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
