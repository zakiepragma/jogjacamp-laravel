<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Category Notification</title>
</head>
<body>
    <h2>Category {{ $action }} successfully!</h2>
    <p>Category Name: {{ $category->name }}</p>
    <p>Is Published: {{ $category->is_publish ? 'Yes' : 'No' }}</p>
</body>
</html>
