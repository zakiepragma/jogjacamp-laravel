<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::query();

        if ($request->has('search')) {
            $search = $request->search;
            $categories->where(function ($query) use ($search) {
                $query->where('name', 'like', "%{$search}%");
            });
        }

        $categories = $categories->paginate(10);

        return view('categories.index', compact('categories'));
    }

    public function create()
    {
        return view('categories.create-category');
    }

    public function edit(Category $category)
    {
        return view('categories.edit-category', compact('category'));
    }

    public function store(CategoryRequest $request, Category $category)
    {
        $category->name = $request->input('name');
        $category->is_publish = $request->input('is_publish') ?? false;
        $category->save();

        return Redirect::route('categories.index')->with('success', 'Category created successfully');
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $category->name = $request->input('name');
        $category->is_publish = $request->input('is_publish') ?? false;
        $category->save();

        return Redirect::route('categories.index')->with('success', 'Category updated successfully');
    }


    public function show(Category $category)
    {
        return view('categories.show-category', compact('category'));
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return Redirect::route('categories.index')->with('success', 'Category has been deleted successfully!');
    }
}
