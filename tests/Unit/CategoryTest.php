<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Category;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_category()
    {
        $category = Category::factory()->make()->toArray();

        $this->postJson('/api/categories', $category)
            ->assertStatus(201)
            ->assertJson(['success' => true]);
    }

    /** @test */
    public function can_show_category()
    {
        $category = Category::factory()->create();

        $this->getJson('/api/categories/' . $category->id)
            ->assertStatus(200)
            ->assertJson(['success' => true]);
    }

    /** @test */
    public function can_update_category()
    {
        $category = Category::factory()->create();

        $updatedData = [
            'name' => 'Updated Category Name',
            'is_publish' => 1
        ];

        $this->putJson('/api/categories/' . $category->id, $updatedData)
            ->assertStatus(200)
            ->assertJson(['success' => true]);

        $this->assertDatabaseHas('categories', $updatedData);
    }

    /** @test */
    // public function can_delete_category()
    // {
    //     $category = Category::factory()->create();

    //     $this->deleteJson('/api/categories/' . $category->id)
    //         ->assertStatus(200)
    //         ->assertJson(['success' => true]);

    //     $this->assertSoftDeleted('categories', ['id' => $category->id]);
    // }
}
